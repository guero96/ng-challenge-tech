import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path:'', redirectTo:'/client', pathMatch:'full'
  },
  { 
    path: 'client', loadChildren: () => import('./components/client/list-client/list-client.module').then(m => m.ListClientModule) 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
