import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { ClientI } from '../../../shared/models/client.interface';
import { ClientService} from '../../../shared/services/client.service';

@Component({
  selector: 'app-admin-client',
  templateUrl: './admin-client.component.html',
  styleUrls: ['./admin-client.component.scss']
})
export class AdminClientComponent implements OnInit {

  @Input() client: ClientI;

  constructor(private clientSvc: ClientService) { }

  public saveClientForm = new FormGroup({
    id:new FormControl(''),
    name: new FormControl('',[Validators.required, Validators.pattern('^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$')]),
    lastName: new FormControl('',[Validators.required, Validators.pattern('^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$')]),
    age: new FormControl('',[Validators.required, Validators.maxLength(2), Validators.pattern('^[0-9]*$')]),
  });

  ngOnInit() {
    this.initValuesForm();
  }

  saveClient(data: ClientI) {
    if (this.saveClientForm.valid) {
      if (data.id) {
        this.clientSvc.updateClient(data);
      } else {
        this.clientSvc.addClient(data);
      }
    }
  }

  private initValuesForm(): void{
    if(this.client){
      this.saveClientForm.patchValue({
        id : this.client.id,
        name: this.client.name,
        lastName: this.client.lastName,
        age: this.client.age,
      });
    }
  }
}
