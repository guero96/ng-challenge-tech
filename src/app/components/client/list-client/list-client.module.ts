import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListClientRoutingModule } from './list-client-routing.module';
import { ListClientComponent } from './list-client.component';
import { MaterialModule } from '../../../material.module';
import { TableComponent } from '../../../shared/components/table/table.component';

@NgModule({
  declarations: [ListClientComponent,TableComponent],
  imports: [
    CommonModule,
    ListClientRoutingModule,
    MaterialModule
  ]
})
export class ListClientModule { }
