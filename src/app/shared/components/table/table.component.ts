import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { ClientService } from '../../../shared/services/client.service';
import { ClientI } from '../../models/client.interface';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './../modal/modal.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['name', 'lastName', 'age','actions'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  average: string;
  standardDesviation: string;

  constructor(
    private clientSvc: ClientService, 
    public dialog:MatDialog ) { }

  ngOnInit() {
    this.clientSvc.getClients().subscribe(clients => {
      this.dataSource.data = clients;

      this.average = this.avg(clients).toFixed(2);

      this.standardDesviation = Math.sqrt(this.variance(clients.map(element => {
        return element.age
      }))).toFixed(2);
    });
  }

  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onAddClient(){
    this.openDialog();
  }

  onUpdateClient(client:ClientI){
    this.openDialog(client);
  }
  
  onDeleteClient(client:ClientI){
    Swal.fire({
      title:'¿Esta seguro de eliminar este cliente?',
      text:`Esta acción no puede revertirse`,
      icon:'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor:'#d33',
      confirmButtonText:'Eliminar',
      cancelButtonText:'Cancelar'
    }).then(result => {
      if(result.value){
        this.clientSvc.deleteClient(client.id).then(() => {
          Swal.fire('Eliminado!!','El cliente ha sido eliminado','success')
        }).catch((e) => {
          Swal.fire('Error!!','Ocurrio un error al eliminar cliente','error')
        })
      }
    });
  }

  openDialog(client?: ClientI): void{
    const config = {
      data:{
        message: client ? 'Editar Cliente' : 'Agregar Cliente',
        content: client
      }
    };
    
    const dialogRef = this.dialog.open(ModalComponent,config);
    dialogRef.afterClosed().subscribe(result => {
     if(result){
      Swal.fire('Exitoso!!','El cliente ha sido guardado exitosamente.','success')
     }
    });
  }

  private avg(data: ClientI[]): number{
    let totalAge=0;
    if(data.length>0){
      data.forEach(element => {
        totalAge += Number(element.age); 
      });
      return totalAge/this.dataSource.data.length;
    }else{
      return 0;
    }
  }

  private isNum(args) {
    args = args.toString();
    if (args.length == 0) return false;
    for (var i = 0; i < args.length; i++) {
      if ((args.substring(i, i + 1) < "0" || args.substring(i, i + 1) > "9") && args.substring(i, i + 1) != "." && args.substring(i, i + 1) != "-") {
        return false;
      }
    }
    return true;
  }

  private variance(arr) {
    var len = 0;
    var sum = 0;
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] == "") { }
      else if (!this.isNum(arr[i])) {
        alert(arr[i] + " is not number, Variance Calculation failed!");
        return 0;
      }
      else {
        len = len + 1;
        sum = sum + parseFloat(arr[i]);
      }
    }
    var v = 0;
    if (len > 1) {
      var mean = sum / len;
      for (var i = 0; i < arr.length; i++) {
        if (arr[i] == "") { }
        else {
          v = v + (arr[i] - mean) * (arr[i] - mean);
        }
      }
      return v / len;
    }
    else {
      return 0;
    }
  }
}
