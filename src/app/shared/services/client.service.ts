import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClientI} from '../../shared/models/client.interface';
import { toPublicName } from '@angular/compiler/src/i18n/serializers/xmb';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
// var FieldValue = require('firebase-admin').firestore.FieldValue;
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private clientCollection: AngularFirestoreCollection<ClientI>;

  constructor(private afs: AngularFirestore) { 
    this.clientCollection = afs.collection<ClientI>('clients', ref => ref.orderBy('createdAt','desc'));
  }

  public getClients(): Observable<ClientI[]>{
    return this.clientCollection
    .snapshotChanges()
    .pipe(
      map(actions =>
          actions.map(a=>{
            const data = a.payload.doc.data() as ClientI;
            const id = a.payload.doc.id;
            return {id, ...data};
          })

        )
    )
  }

  public updateClient(client:ClientI){
    const clientObj = {
      name : client.name,
      lastName: client.lastName,
      age: client.age
    };
    return this.clientCollection.doc(client.id).update(clientObj);
  }

  public deleteClient(id:string){
    return this.clientCollection.doc(id).delete();
  }

  public addClient(client: ClientI){
    const clientObj = {
      name : client.name,
      lastName: client.lastName,
      age: client.age,
      createdAt: firebase.firestore.FieldValue.serverTimestamp()
    };
    this.clientCollection.add(clientObj);
  }

}
