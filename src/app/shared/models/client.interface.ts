export interface ClientI{
  id?: string;
  name: string;
  lastName: string;
  age: number;
}