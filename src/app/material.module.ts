import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatIconModule,
  MatTableModule, 
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule
} from '@angular/material';

const myModule = [  
  MatInputModule,
  MatFormFieldModule,
  MatButtonModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule, 
  MatSortModule,
  MatDialogModule
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    myModule
  ],
  exports : [myModule]
})
export class MaterialModule { }
