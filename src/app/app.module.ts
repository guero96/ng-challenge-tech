import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';

/*Firebase*/
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';

import { environment } from 'src/environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalComponent } from './shared/components/modal/modal.component';
import { AdminClientComponent } from './components/client/admin-client/admin-client.component';
import { AdminClientModule } from './components/client/admin-client/admin-client.module';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    AdminClientComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment),
    AngularFirestoreModule,
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
    AdminClientModule
  ],
  entryComponents: [ModalComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
