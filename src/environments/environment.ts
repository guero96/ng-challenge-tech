// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyCxnHr1ULSPUecenqlQ4l3zJblgIgVz1UA",
  authDomain: "ngchallengetech.firebaseapp.com",
  databaseURL: "https://ngchallengetech.firebaseio.com",
  projectId: "ngchallengetech",
  storageBucket: "ngchallengetech.appspot.com",
  messagingSenderId: "688178976846",
  appId: "1:688178976846:web:2e628ad12bed7a54300b1f",
  measurementId: "G-RPNF3VYFZL"

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
